"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var index_1 = require("../../_services/index");
var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(dashboardService, router) {
        this.dashboardService = dashboardService;
        this.router = router;
        this.fetchAllTickets();
    }
    DashboardComponent.prototype.ngOnInit = function () {
    };
    DashboardComponent.prototype.fetchAllTickets = function () {
        var _this = this;
        this.dashboardService.getDataForDashboard()
            .subscribe(function (result) {
            if (result) {
                result.forEach(function (element) {
                    element.CreatedOn = new Date(+element.CreatedOn.split("(")[1].split(")")[0]);
                });
                _this.tickets = result;
            }
        }, function (error) {
            if (error.status === 400) {
                _this.router.navigate(["/login"]);
            }
            else {
                console.error(error);
            }
        });
    };
    DashboardComponent.prototype.add = function () {
        var len = this.tickets.length;
        this.editRowId = this.tickets[len - 1].BugId + 1;
        var date = new Date;
        this.tickets.push({ BugId: len + 1, Subject: "", ReportedBy: "", AssignedTo: "", CreatedOn: date, Priority: "", Status: "open", Comments: "" });
    };
    DashboardComponent.prototype.delete = function (i) {
        this.tickets.splice(i, 1);
    };
    DashboardComponent.prototype.edit = function (i) {
        if (this.editRowId === i + 1)
            this.editRowId = null;
        else
            this.editRowId = i + 1;
    };
    DashboardComponent.prototype.save = function () {
        var _this = this;
        this.dashboardService.saveTickets(this.tickets)
            .subscribe(function (result) {
            if (result) {
                _this.fetchAllTickets();
                setTimeout(function () {
                    alert("Saved successfully!");
                }, 300);
            }
            else {
                alert("Try again later");
            }
        }, function (error) {
            if (error.status === 400) {
                _this.router.navigate(["/login"]);
            }
            else {
                console.error(error);
            }
        });
    };
    DashboardComponent = __decorate([
        core_1.Component({
            selector: 'app-dashboard',
            templateUrl: './dashboard.component.html',
            styleUrls: ['./dashboard.component.css']
        }),
        __metadata("design:paramtypes", [index_1.DashboardService, router_1.Router])
    ], DashboardComponent);
    return DashboardComponent;
}());
exports.DashboardComponent = DashboardComponent;
//# sourceMappingURL=dashboard.component.js.map