﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BugReportManagement.Startup))]
namespace BugReportManagement
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            CreateMap();
        }
    }
}
