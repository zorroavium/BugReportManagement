﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BugReportManagement.Models
{
    public class SubscriptionViewModel
    {
        public string EndPoint { get; set; }
        public int ExpirationTime { get; set; }
        public Key Keys { get; set; }
    }
}