﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BugReportManagement.Models
{
    public class Key
    {
        public string P256DH { get; set; }
        public string Auth { get; set; }
    }
}