using BRM.Data;
using BRM.Domain.Interfaces.Repositories;
using BRM.Domain.Interfaces.Services;
using BRM.Repository;
using BRM.Service;
using System.Data.Entity;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace BugReportManagement
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            container.RegisterType<IUnitOfWork, UnitOfWork>();
            container.RegisterType<DbContext, BRMDBContext>();
            container.RegisterType<ITicketService,TicketService>();
            container.RegisterType<ITicketRepository,TicketRepository>();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}