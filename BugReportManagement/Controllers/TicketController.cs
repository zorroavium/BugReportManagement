﻿using AutoMapper;
using BRM.Domain.Entities;
using BRM.Domain.Interfaces.Services;
using BRM.Domain.ServiceModels;
using BugReportManagement.Attribute;
using BugReportManagement.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebPush;

namespace BugReportManagement.Controllers
{
    public class TicketController : Controller
    {
        private readonly ITicketService _ticketService;

        public TicketController(ITicketService ticketService)
        {
            _ticketService = ticketService;
        }

        // GET: Ticket
        [AllowCrossSiteJson]
        [HttpGet]
        public JsonResult GetAllTickets()
        {
            var data = _ticketService.GetAllTickets();
            return new JsonResult { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [AllowCrossSiteJson]
        [HttpPost]
        public ActionResult Save(IEnumerable<Tickets> tickets)
        {
            var res = _ticketService.SaveTickets(tickets);
            if (res)
                NotifyUsers();
            return new JsonResult { Data = res, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        private string NotifyUsers()
        {
            //var keys = VapidHelper.GenerateVapidKeys();
            //keys.PrivateKey;
            //keys.PublicKey;

            var vapidDetails = new VapidDetails(
                @"mailto:coolrp119@gmail.com",
                "BBR1f3YtSfoeNRqrwKsqCBvZIT6x5uaXQK07GizscpQMce7ZnXb6E0gckhfRgovi0vnbt90rEheF2PTjn7AlZZk",
                "KYUVv67iQCXoSht-1u32HGWzTSD9CBW1hZFD5Oor3MU"
                );

            var subscriptions = _ticketService.GetAllSubscription();
            var payload = "Data Updated";

            var webPushClient = new WebPushClient();
            foreach (var subscription in subscriptions.Select(s => new PushSubscription(s.Subscription,s.P256DH,s.Auth)))
            {
                try
                {
                    webPushClient.SendNotification(subscription, payload, vapidDetails);
                }
                catch (WebPushException ex)
                {
                    throw;
                }
            }
            return "";
        }

        [AllowCrossSiteJson]
        [HttpPost]
        public ActionResult SaveSubscription(SubscriptionViewModel subscription)
        {
            var newSubscription = Mapper.Map<SubscriptionViewModel,Subscription>(subscription);
            var res = _ticketService.SaveSubsciption(newSubscription);
            return new JsonResult { Data = res, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

        }
    }
}