webpackJsonp(["layout.module"],{

/***/ "../../../../../src/app/shared/layout/footer/footer.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "", "", {"version":3,"sources":[],"names":[],"mappings":"","file":"footer.component.css","sourceRoot":""}]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/layout/footer/footer.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  footer works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/shared/layout/footer/footer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-footer',
            template: __webpack_require__("../../../../../src/app/shared/layout/footer/footer.component.html"),
            styles: [__webpack_require__("../../../../../src/app/shared/layout/footer/footer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "../../../../../src/app/shared/layout/header/header.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "", "", {"version":3,"sources":[],"names":[],"mappings":"","file":"header.component.css","sourceRoot":""}]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/layout/header/header.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\n  <nav class=\"navbar navbar-dark bg-dark\">\n    <a class=\"navbar-brand text-warning ml-3\">Bug Report Management</a>\n    <form class=\"form-inline text-warning\">\n        <i class=\"fa fa-envelope\"></i>\n        <i class=\"fa fa-bell ml-4 push-btn\"></i> \n        <i class=\"fa fa-user ml-4 mr-3\"></i>\n    </form>\n  </nav>\n</div>"

/***/ }),

/***/ "../../../../../src/app/shared/layout/header/header.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent.prototype.toggleSidebar = function () {
        var dom = document.querySelector("body");
        dom.classList.toggle("push-right");
    };
    HeaderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-header',
            template: __webpack_require__("../../../../../src/app/shared/layout/header/header.component.html"),
            styles: [__webpack_require__("../../../../../src/app/shared/layout/header/header.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "../../../../../src/app/shared/layout/layout-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LayoutRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__layout_component__ = __webpack_require__("../../../../../src/app/shared/layout/layout.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: "",
        component: __WEBPACK_IMPORTED_MODULE_2__layout_component__["a" /* LayoutComponent */],
        children: [
            // { path: "", component:LayoutComponent },
            { path: "dashboard", loadChildren: "app/module/dashboard/dashboard.module#DashboardModule" },
        ],
    },
];
var LayoutRoutingModule = /** @class */ (function () {
    function LayoutRoutingModule() {
    }
    LayoutRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* RouterModule */]],
        })
    ], LayoutRoutingModule);
    return LayoutRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/shared/layout/layout.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "", "", {"version":3,"sources":[],"names":[],"mappings":"","file":"layout.component.css","sourceRoot":""}]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/layout/layout.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"height:100%\">\n  <app-header></app-header>\n  <app-sidebar></app-sidebar>\n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/shared/layout/layout.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LayoutComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LayoutComponent = /** @class */ (function () {
    function LayoutComponent(router) {
        this.router = router;
    }
    LayoutComponent.prototype.ngOnInit = function () {
        this.router.navigate(["/dashboard"]);
    };
    LayoutComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-layout',
            template: __webpack_require__("../../../../../src/app/shared/layout/layout.component.html"),
            styles: [__webpack_require__("../../../../../src/app/shared/layout/layout.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]])
    ], LayoutComponent);
    return LayoutComponent;
}());



/***/ }),

/***/ "../../../../../src/app/shared/layout/layout.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutModule", function() { return LayoutModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__layout_component__ = __webpack_require__("../../../../../src/app/shared/layout/layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__header_header_component__ = __webpack_require__("../../../../../src/app/shared/layout/header/header.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__sidebar_sidebar_component__ = __webpack_require__("../../../../../src/app/shared/layout/sidebar/sidebar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__footer_footer_component__ = __webpack_require__("../../../../../src/app/shared/layout/footer/footer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__layout_routing_module__ = __webpack_require__("../../../../../src/app/shared/layout/layout-routing.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var LayoutModule = /** @class */ (function () {
    function LayoutModule() {
    }
    LayoutModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */], __WEBPACK_IMPORTED_MODULE_6__layout_routing_module__["a" /* LayoutRoutingModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__layout_component__["a" /* LayoutComponent */], __WEBPACK_IMPORTED_MODULE_3__header_header_component__["a" /* HeaderComponent */], __WEBPACK_IMPORTED_MODULE_4__sidebar_sidebar_component__["a" /* SidebarComponent */], __WEBPACK_IMPORTED_MODULE_5__footer_footer_component__["a" /* FooterComponent */]],
        })
    ], LayoutModule);
    return LayoutModule;
}());



/***/ }),

/***/ "../../../../../src/app/shared/layout/sidebar/sidebar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(true);
// imports


// module
exports.push([module.i, "/*! CSS Used from: http://demo.themefisher.com/themefisher/focus/assets/css/lib/font-awesome.min.css */\r\n.fa{display:inline-block;font:normal normal normal 14px/1 FontAwesome;font-size:inherit;text-rendering:auto;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}\r\n.fa-usd:before{content:\"\\F155\";}\r\n/*! CSS Used from: http://demo.themefisher.com/themefisher/focus/assets/css/lib/bootstrap.min.css */\r\n@media print{\r\n*,::after,::before{text-shadow:none!important;-webkit-box-shadow:none!important;box-shadow:none!important;}\r\n}\r\n*,::after,::before{-webkit-box-sizing:inherit;box-sizing:inherit;}\r\n.row{display:-ms-flexbox;display:-webkit-box;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px;}\r\n.col-lg-3{position:relative;width:100%;min-height:1px;padding-right:15px;padding-left:15px;}\r\n@media (min-width:992px){\r\n.col-lg-3{-ms-flex:0 0 25%;-webkit-box-flex:0;flex:0 0 25%;max-width:25%;}\r\n}\r\n.w-50{width:50%!important;}\r\n.w-75{width:75%!important;}\r\n/*! CSS Used from: http://demo.themefisher.com/themefisher/focus/assets/css/lib/helper.css */\r\n.w-50{width:50%!important;}\r\n.w-65{width:65%!important;}\r\n.w-75{width:75%!important;}\r\n.w-85{width:85%!important;}\r\n/*! CSS Used from: http://demo.themefisher.com/themefisher/focus/assets/css/style.css */\r\n.card{background:#ffffff;margin:7.5px 0;padding:20px;border:1px solid #e7e7e7;border-radius:3px;-webkit-box-shadow:0 5px 20px rgba(0, 0, 0, 0.1);box-shadow:0 5px 20px rgba(0, 0, 0, 0.1);}\r\n.stat-widget-two{text-align:center;}\r\n.stat-widget-two .stat-digit{font-size:1.75rem;font-weight:500;color:#373757;}\r\n.stat-widget-two .stat-digit i{font-size:18px;margin-right:5px;}\r\n.stat-widget-two .stat-text{font-size:16px;margin-bottom:5px;color:#868e96;}\r\n.stat-widget-two .progress{height:8px;margin-bottom:0;margin-top:20px;-webkit-box-shadow:none;box-shadow:none;}\r\n.stat-widget-two .progress-bar{-webkit-box-shadow:none;box-shadow:none;}\r\n@media (max-width: 768px){\r\n.card{display:inline-block;width:100%;}\r\n}\r\n@media (max-width: 360px){\r\n.stat-widget-two .stat-digit{font-size:16px;}\r\n.stat-widget-two .stat-text{font-size:14px;}\r\n}\r\n.progress-bar{background-color:#5873fe;}\r\n.progress-bar-primary{background-color:#5873fe;}\r\n.progress-bar-success{background-color:#28a745;}\r\n.progress-bar-danger{background-color:#dc3545;}\r\n.progress-bar-warning{background-color:#e7b63a;}\r\n.row{margin-left:-7.5px;margin-right:-7.5px;}\r\n.col-lg-3{padding-top:7.5px;padding-bottom:7.5px;}", "", {"version":3,"sources":["C:/Users/rahulp/documents/visual studio 2015/Projects/BugReportManagement/BugReportManagement/BRMClient/src/app/shared/layout/sidebar/C:/Users/rahulp/documents/visual studio 2015/Projects/BugReportManagement/BugReportManagement/BRMClient/src/app/shared/layout/sidebar/sidebar.component.css"],"names":[],"mappings":"AAAA,uGAAuG;AACvG,IAAI,qBAAqB,6CAA6C,kBAAkB,oBAAoB,mCAAmC,kCAAkC,CAAC;AAClL,eAAe,gBAAgB,CAAC;AAChC,oGAAoG;AACpG;AACA,mBAAmB,2BAA2B,kCAAA,0BAA0B,CAAC;CACxE;AACD,mBAAmB,2BAAA,mBAAmB,CAAC;AACvC,KAAK,oBAAoB,oBAAA,aAAa,mBAAmB,eAAe,mBAAmB,kBAAkB,CAAC;AAC9G,UAAU,kBAAkB,WAAW,eAAe,mBAAmB,kBAAkB,CAAC;AAC5F;AACA,UAAU,iBAAiB,mBAAA,aAAa,cAAc,CAAC;CACtD;AACD,MAAM,oBAAoB,CAAC;AAC3B,MAAM,oBAAoB,CAAC;AAC3B,6FAA6F;AAC7F,MAAM,oBAAoB,CAAC;AAC3B,MAAM,oBAAoB,CAAC;AAC3B,MAAM,oBAAoB,CAAC;AAC3B,MAAM,oBAAoB,CAAC;AAC3B,wFAAwF;AACxF,MAAM,mBAAmB,eAAe,aAAa,yBAAyB,kBAAkB,iDAAA,yCAAyC,CAAC;AAC1I,iBAAiB,kBAAkB,CAAC;AACpC,6BAA6B,kBAAkB,gBAAgB,cAAc,CAAC;AAC9E,+BAA+B,eAAe,iBAAiB,CAAC;AAChE,4BAA4B,eAAe,kBAAkB,cAAc,CAAC;AAC5E,2BAA2B,WAAW,gBAAgB,gBAAgB,wBAAA,gBAAgB,CAAC;AACvF,+BAA+B,wBAAA,gBAAgB,CAAC;AAChD;AACA,MAAM,qBAAqB,WAAW,CAAC;CACtC;AACD;AACA,6BAA6B,eAAe,CAAC;AAC7C,4BAA4B,eAAe,CAAC;CAC3C;AACD,cAAc,yBAAyB,CAAC;AACxC,sBAAsB,yBAAyB,CAAC;AAChD,sBAAsB,yBAAyB,CAAC;AAChD,qBAAqB,yBAAyB,CAAC;AAC/C,sBAAsB,yBAAyB,CAAC;AAChD,KAAK,mBAAmB,oBAAoB,CAAC;AAC7C,UAAU,kBAAkB,qBAAqB,CAAC","file":"sidebar.component.css","sourcesContent":["/*! CSS Used from: http://demo.themefisher.com/themefisher/focus/assets/css/lib/font-awesome.min.css */\r\n.fa{display:inline-block;font:normal normal normal 14px/1 FontAwesome;font-size:inherit;text-rendering:auto;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}\r\n.fa-usd:before{content:\"\\f155\";}\r\n/*! CSS Used from: http://demo.themefisher.com/themefisher/focus/assets/css/lib/bootstrap.min.css */\r\n@media print{\r\n*,::after,::before{text-shadow:none!important;box-shadow:none!important;}\r\n}\r\n*,::after,::before{box-sizing:inherit;}\r\n.row{display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px;}\r\n.col-lg-3{position:relative;width:100%;min-height:1px;padding-right:15px;padding-left:15px;}\r\n@media (min-width:992px){\r\n.col-lg-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%;}\r\n}\r\n.w-50{width:50%!important;}\r\n.w-75{width:75%!important;}\r\n/*! CSS Used from: http://demo.themefisher.com/themefisher/focus/assets/css/lib/helper.css */\r\n.w-50{width:50%!important;}\r\n.w-65{width:65%!important;}\r\n.w-75{width:75%!important;}\r\n.w-85{width:85%!important;}\r\n/*! CSS Used from: http://demo.themefisher.com/themefisher/focus/assets/css/style.css */\r\n.card{background:#ffffff;margin:7.5px 0;padding:20px;border:1px solid #e7e7e7;border-radius:3px;box-shadow:0 5px 20px rgba(0, 0, 0, 0.1);}\r\n.stat-widget-two{text-align:center;}\r\n.stat-widget-two .stat-digit{font-size:1.75rem;font-weight:500;color:#373757;}\r\n.stat-widget-two .stat-digit i{font-size:18px;margin-right:5px;}\r\n.stat-widget-two .stat-text{font-size:16px;margin-bottom:5px;color:#868e96;}\r\n.stat-widget-two .progress{height:8px;margin-bottom:0;margin-top:20px;box-shadow:none;}\r\n.stat-widget-two .progress-bar{box-shadow:none;}\r\n@media (max-width: 768px){\r\n.card{display:inline-block;width:100%;}\r\n}\r\n@media (max-width: 360px){\r\n.stat-widget-two .stat-digit{font-size:16px;}\r\n.stat-widget-two .stat-text{font-size:14px;}\r\n}\r\n.progress-bar{background-color:#5873fe;}\r\n.progress-bar-primary{background-color:#5873fe;}\r\n.progress-bar-success{background-color:#28a745;}\r\n.progress-bar-danger{background-color:#dc3545;}\r\n.progress-bar-warning{background-color:#e7b63a;}\r\n.row{margin-left:-7.5px;margin-right:-7.5px;}\r\n.col-lg-3{padding-top:7.5px;padding-bottom:7.5px;}"],"sourceRoot":""}]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/layout/sidebar/sidebar.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid sidebar\">\n  <div class=\"row\" style=\"height:auto\">\n      <div class=\"col-md-3 col-xs-1 p-l-0 p-r-0 collapse in\" id=\"sidebar\">\n          <div class=\"list-group panel\">\n              <a href=\"#menu1\" class=\"list-group-item collapsed\" data-toggle=\"collapse\" data-parent=\"#sidebar\" aria-expanded=\"false\">\n                <i class=\"fa fa-dashboard mr-2\"></i> <span class=\"hidden-sm-down\">All unsolved tickets</span> </a>\n              <div class=\"collapse\" id=\"menu1\">\n                  <a href=\"#menu1sub1\" class=\"list-group-item\" data-toggle=\"collapse\" aria-expanded=\"false\">Subitem 1 </a>\n                  <div class=\"collapse\" id=\"menu1sub1\">\n                      <a href=\"#\" class=\"list-group-item\" data-parent=\"#menu1sub1\">Subitem 1 a</a>\n                      <a href=\"#\" class=\"list-group-item\" data-parent=\"#menu1sub1\">Subitem 2 b</a>\n                      <a href=\"#menu1sub1sub1\" class=\"list-group-item\" data-toggle=\"collapse\" aria-expanded=\"false\">Subitem 3 c </a>\n                      <div class=\"collapse\" id=\"menu1sub1sub1\">\n                          <a href=\"#\" class=\"list-group-item\" data-parent=\"#menu1sub1sub1\">Subitem 3 c.1</a>\n                          <a href=\"#\" class=\"list-group-item\" data-parent=\"#menu1sub1sub1\">Subitem 3 c.2</a>\n                      </div>\n                      <a href=\"#\" class=\"list-group-item\" data-parent=\"#menu1sub1\">Subitem 4 d</a>\n                      <a href=\"#menu1sub1sub2\" class=\"list-group-item\" data-toggle=\"collapse\"  aria-expanded=\"false\">Subitem 5 e </a>\n                      <div class=\"collapse\" id=\"menu1sub1sub2\">\n                          <a href=\"#\" class=\"list-group-item\" data-parent=\"#menu1sub1sub2\">Subitem 5 e.1</a>\n                          <a href=\"#\" class=\"list-group-item\" data-parent=\"#menu1sub1sub2\">Subitem 5 e.2</a>\n                      </div>\n                  </div>\n                  <a href=\"#\" class=\"list-group-item\" data-parent=\"#menu1\">Subitem 2</a>\n                  <a href=\"#\" class=\"list-group-item\" data-parent=\"#menu1\">Subitem 3</a>\n              </div>\n              <a href=\"#\" class=\"list-group-item collapsed\" data-parent=\"#sidebar\"><i class=\"fa fa-film mr-2\"></i> <span class=\"hidden-sm-down\">Your unsolved tickets</span></a>\n              <a href=\"#menu3\" class=\"list-group-item collapsed\" data-toggle=\"collapse\" data-parent=\"#sidebar\" aria-expanded=\"false\">\n                <i class=\"fa fa-book mr-2\">\n\n              </i> <span class=\"hidden-sm-down\">Unassigned tickets</span></a>\n              <div class=\"collapse\" id=\"menu3\">\n                  <a href=\"#\" class=\"list-group-item\" data-parent=\"#menu3\">3.1</a>\n                  <a href=\"#menu3sub2\" class=\"list-group-item\" data-toggle=\"collapse\" aria-expanded=\"false\">3.2 </a>\n                  <div class=\"collapse\" id=\"menu3sub2\">\n                      <a href=\"#\" class=\"list-group-item\" data-parent=\"#menu3sub2\">3.2 a</a>\n                      <a href=\"#\" class=\"list-group-item\" data-parent=\"#menu3sub2\">3.2 b</a>\n                      <a href=\"#\" class=\"list-group-item\" data-parent=\"#menu3sub2\">3.2 c</a>\n                  </div>\n                  <a href=\"#\" class=\"list-group-item\" data-parent=\"#menu3\">3.3</a>\n              </div>\n              <a href=\"#\" class=\"list-group-item collapsed\" data-parent=\"#sidebar\"><i class=\"fa fa-ticket mr-2\"></i> <span class=\"hidden-sm-down\">New Tickets</span></a>\n              <a href=\"#\" class=\"list-group-item collapsed\" data-parent=\"#sidebar\"><i class=\"fa fa-list mr-2\"></i> <span class=\"hidden-sm-down\">Recently updated tickets</span></a>\n              <a [routerLink]=\"['/allTickets']\" [routerLinkActive]=\"['router-link-active']\" [routerLinkActiveOptions]=\"{exact: true}\"\n               class=\"list-group-item collapsed\" data-parent=\"#sidebar\"><i class=\"fa fa-clock-o mr-2\"></i> <span class=\"hidden-sm-down\">All tickets</span></a>\n              <a href=\"#\" class=\"list-group-item collapsed\" data-parent=\"#sidebar\"><i class=\"fa fa-th mr-2\"></i> <span class=\"hidden-sm-down\">Link</span></a>\n              <a href=\"#\" class=\"list-group-item collapsed\" data-parent=\"#sidebar\"><i class=\"fa fa-gear mr-2\"></i> <span class=\"hidden-sm-down\">Link</span></a>\n          </div>\n      </div>\n      <main class=\"col-md-9 col-xs-11 p-l-2 p-t-2\">\n          <a href=\"#sidebar\" data-toggle=\"collapse\"><i class=\"fa fa-navicon fa-lg\"></i></a>\n          <hr>\n          <ol class=\"breadcrumb\">\n              <li class=\"breadcrumb-item\"><a href=\"#\">Bug Report Management</a></li>\n              <li class=\"breadcrumb-item active\">Dashboard</li>\n            </ol>\n          <div class=\"page-header\">\n            <div class=\"row\">\n              <div class=\"col-lg-3\">\n                  <div class=\"card\">\n                      <div class=\"stat-widget-two\">\n                          <div class=\"stat-content\">\n                              <div class=\"stat-text text-success\">Task Completed </div>\n                              <div class=\"stat-digit\"> <i class=\"fa fa-ticket\"></i>8</div>\n                          </div>\n                          <div class=\"progress\">\n                              <div class=\"progress-bar progress-bar-success w-85\" role=\"progressbar\" aria-valuenow=\"85\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n                          </div>\n                      </div>\n                  </div>\n              </div>\n              <div class=\"col-lg-3\">\n                  <div class=\"card\">\n                      <div class=\"stat-widget-two\">\n                          <div class=\"stat-content\">\n                              <div class=\"stat-text text-primary\">New Tickets</div>\n                              <div class=\"stat-digit\"> <i class=\"fa fa-ticket\"></i>7</div>\n                          </div>\n                          <div class=\"progress\">\n                              <div class=\"progress-bar progress-bar-primary w-75\" role=\"progressbar\" aria-valuenow=\"78\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n                          </div>\n                      </div>\n                  </div>\n              </div>\n              <div class=\"col-lg-3\">\n                  <div class=\"card\">\n                      <div class=\"stat-widget-two\">\n                          <div class=\"stat-content\">\n                              <div class=\"stat-text text-warning\">Pending Tickets</div>\n                              <div class=\"stat-digit\"> <i class=\"fa fa-tasks\"></i> 5</div>\n                          </div>\n                          <div class=\"progress\">\n                              <div class=\"progress-bar progress-bar-warning w-50\" role=\"progressbar\" aria-valuenow=\"50\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n                          </div>\n                      </div>\n                  </div>\n              </div>\n              <div class=\"col-lg-3\">\n                  <div class=\"card\">\n                      <div class=\"stat-widget-two\">\n                          <div class=\"stat-content\">\n                              <div class=\"stat-text text-danger\">Unsolved Tickets</div>\n                              <div class=\"stat-digit\"> <i class=\"fa fa-ticket\"></i>6</div>\n                          </div>\n                          <div class=\"progress\">\n                              <div class=\"progress-bar progress-bar-danger w-65\" role=\"progressbar\" aria-valuenow=\"65\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n                          </div>\n                      </div>\n                  </div>\n                  <!-- /# card -->\n              </div>\n              <!-- /# column -->\n          </div>\n              <section class=\"main-container\">\n                <router-outlet></router-outlet>               \n              </section>\n          </div>          \n      </main>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/shared/layout/sidebar/sidebar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SidebarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SidebarComponent = /** @class */ (function () {
    function SidebarComponent() {
    }
    SidebarComponent.prototype.ngOnInit = function () {
    };
    SidebarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-sidebar',
            template: __webpack_require__("../../../../../src/app/shared/layout/sidebar/sidebar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/shared/layout/sidebar/sidebar.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SidebarComponent);
    return SidebarComponent;
}());



/***/ })

});
//# sourceMappingURL=layout.module.chunk.js.map