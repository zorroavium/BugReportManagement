
'use strict';
const applicationServerPublicKey = 'BBR1f3YtSfoeNRqrwKsqCBvZIT6x5uaXQK07GizscpQMce7ZnXb6E0gckhfRgovi0vnbt90rEheF2PTjn7AlZZk';
const pushButton = document.querySelector('.push-btn');
let isSubscribed = false;
let swRegistration = null;
let promptEvt;

if ('serviceWorker' in navigator && 'PushManager' in window) {
    //pushButton = document.querySelector('.push-btn');
    console.log('Service Worker and Push is supported');
  
    navigator.serviceWorker.register('sw.js')
    .then(function(swReg) {
      console.log('Service Worker is registered', swReg);
  
      swRegistration = swReg;
      initializeUI();
    })
    .catch(function(error) {
      console.error('Service Worker Error', error);
    });
  } else {
    console.warn('Push messaging is not supported');
    pushButton.textContent = 'Push Not Supported';
  }

  function initializeUI() {
    // Set the initial subscription value
    pushButton.addEventListener('click', function() {
        pushButton.disabled = true;

        if (isSubscribed) {
          unsubscribeUser();
        } else {
          subscribeUser();
        }

    });

    function subscribeUser() {
        const applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey);
        swRegistration.pushManager.subscribe({
          userVisibleOnly: true,
          applicationServerKey: applicationServerKey
        })
        .then(subscription => fetch('http://localhost/BugReportManagement/Ticket/SaveSubscription',{
            headers: { 'Content-Type': 'application/json'},
            method: 'POST',
            credentials: 'same-origin',
            body: JSON.stringify(subscription)
            }))
            .then(function(res) {
          console.log('User is subscribed.');
      
          //updateSubscriptionOnServer(res);
      
          isSubscribed = true;
      
          updateBtn();
        })
        .catch(function(err) {
          console.log('Failed to subscribe the user: ', err);
          updateBtn();
        });
      }

      function unsubscribeUser() {
        swRegistration.pushManager.getSubscription()
        .then(function(subscription) {
          if (subscription) {
            return subscription.unsubscribe();
          }
        })
        .catch(function(error) {
          console.log('Error unsubscribing', error);
        })
        .then(function() {
         // updateSubscriptionOnServer(null);
            
          console.log('User is unsubscribed.');
          isSubscribed = false;
      
          updateBtn();
        });
      }

      // function updateSubscriptionOnServer(subscription) {
      //   // TODO: Send subscription to application server
      
      //  // const subscriptionJson = document.querySelector('.js-subscription-json');
      
      //   if (subscription) {
      //   //  subscriptionJson.textContent = JSON.stringify(subscription);
      //   }
      //   else{
      //       subscriptionJson.textContent="";
      //   } 
      // }

    swRegistration.pushManager.getSubscription()
    .then(function(subscription) {
      isSubscribed = !(subscription === null);
  
      if (isSubscribed) {
        console.log('User IS subscribed.');
      } else {
        console.log('User is NOT subscribed.');
      }
  
      updateBtn();
    });
  }

  function updateBtn() {

    if (Notification.permission === 'denied') {
        pushButton.textContent = 'Push Messaging Blocked.';
        pushButton.disabled = true;
        //updateSubscriptionOnServer(null);
        return;
      }
    
    if (isSubscribed) {
      pushButton.textContent = 'Disable Push Messaging';
    } else {
      pushButton.textContent = 'Enable Push Messaging';
    }
    pushButton.disabled = false;
  }

  Notification.requestPermission(function(status) {
    console.log('Notification permission status:', status);
});

function urlB64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
      .replace(/\-/g, '+')
      .replace(/_/g, '/');
  
    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);
  
    for (let i = 0; i < rawData.length; ++i) {
      outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
  }
  
  window.addEventListener('appinstalled',evt =>{
    console.log('The app was installed');
});

//Events for web app manifest
window.addEventListener('beforeinstallprompt',evt =>{
    evt.preventDefault();

    // save the event for later
    promptEvt=evt;
    // show install banner now
    promptEvt.prompt();

    promptEvt.userChoice.then(choice => {
        console.log('User choice:', choice.outcome);
    });
   // return false;
});

// //Events for web app manifest
// if(promptEvt){

//     // show install banner now
//     promptEvt.prompt();

//     promptEvt.userChoice.then(choice => {
//         console.log('User choice:', choice.outcome);
//     });
// }

window.indexedDB.open('BRM',1).onsuccess = (evt) =>{
  var db= evt.target.result;
  var tickets =db.transaction(["tickets"],'readwrite').objectStore('tickets');
  var val= {tickets:tickets}; 
  tickets.put(val).success =() => {
    if ('serviceWorker' in navigator && 'SyncManager' in window) {
      navigator.serviceWorker.ready
      .then( sw =>{
        return sw.sync.register('sync-tickets')
        .then( r=> {
          //sync registered! UPdate screen?
        })
      })
      }
      else{
        // TODO: Send data to server via ajax
      }
  }
};

// if ('serviceWorker' in navigator && 'SyncManager' in window) {
// navigator.serviceWorker.ready
// .then( sw =>{
//   return sw.sync.register('sync-tickets')
//   .then( r=> {
//     //sync registered! UPdate screen?
//   })
// })
// }