import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { LayoutComponent } from "./layout.component";

const routes: Routes = [
    {
        path: "",
        component: LayoutComponent,
        children: [
           // { path: "", component:LayoutComponent },
             { path: "dashboard", loadChildren: "app/module/dashboard/dashboard.module#DashboardModule" },
            // { path: "events", loadChildren: "app/events/event.module#EventModule" },
            // { path: "user", loadChildren: "app/user/user.module#UserModule" },
            // { path: "maintenance", loadChildren: "app/maintenance/maintenance.module#MaintenanceModule" },
            // { path: "systemsettings", loadChildren: "app/system-settings/system-settings.module#SystemSettingsModule" },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LayoutRoutingModule {

}

