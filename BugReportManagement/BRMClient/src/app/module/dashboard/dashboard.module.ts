import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { DashboardService } from '../../_services/dashboard.service';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,DashboardRoutingModule,FormsModule
  ],
  declarations: [DashboardComponent],
  providers: [DashboardService]
})
export class DashboardModule { }
