import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { DatePipe } from '@angular/common';
import { DashboardService } from "../../_services/index";
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  tickets:any[];
  editRowId:any;
  constructor(private dashboardService: DashboardService, private router: Router) {
    this.fetchAllTickets();
   }

  ngOnInit() {

  }

  fetchAllTickets(){
    this.dashboardService.getDataForDashboard()
    .subscribe((result) => {
        if (result) {
          result.forEach(element => {
            element.CreatedOn=new Date(+element.CreatedOn.split("(")[1].split(")")[0]);
          });
          this.tickets=result;
        }
    },
    (error) => {
        if (error.status === 400) {
            this.router.navigate(["/login"]);
        } else {
            console.error(error);
        }
    });
  }

  add(){
    var len=this.tickets.length;
    this.editRowId = this.tickets[len - 1].BugId + 1;
    let date=new Date;
    this.tickets.push({BugId:len+1,	Subject:"",	ReportedBy:"",	AssignedTo:"",	CreatedOn:date,	Priority:"",	Status:"open",	Comments:""});
  }

  delete(i){
    this.tickets.splice(i,1);
  }

  edit(i){
    if(this.editRowId === i+1)
    this.editRowId=null;
    else
    this.editRowId=i+1;
  }

  save(){
    this.dashboardService.saveTickets(this.tickets)
    .subscribe((result) => {
      if (result) {
        this.fetchAllTickets();
        setTimeout(() => {
          alert("Saved successfully!")
      }, 300);
      }
      else{
        alert("Try again later");
      }
  },
  (error) => {
      if (error.status === 400) {
          this.router.navigate(["/login"]);
      } else {
          console.error(error);
      }
  });
  }
}
