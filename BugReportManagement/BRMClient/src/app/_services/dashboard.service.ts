import { Injectable } from "@angular/core";
import { Headers, Http, RequestOptions, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";

import "rxjs/add/observable/throw";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";
import "rxjs/add/operator/mergeMap";

@Injectable()
export class DashboardService {

    constructor(private http: Http ) {

    }

    public getDataForDashboard(): Observable<any> {
        return this.http.get("http://localhost/BugReportManagement/Ticket/GetAllTickets")
            .map((response: Response) => {
                return response.json();
            })            
    }

    public saveTickets(tickets): Observable<any> {
        return this.http.post("http://localhost/BugReportManagement/Ticket/Save",tickets)
            .map((response: Response) => {
                return response.json();
            })            
    }
    
}
