
const VERSION='1';

function log(messages){
console.log(VERSION,messages);
}

log('Installing Service Worker');

self.addEventListener('install',function(event) {
    // event.waitUntil(
    //     // return promise to install sw
    // );
    log('version is installed');
});

self.addEventListener('activate',function(event) {
    // event.waitUntil(
    //     // return promise to activate sw
    // );
    log('version is activated');
});

self.addEventListener('fetch',function(event){
    if(!navigator.onLine)
    {
        event.respondWith(new Response('<h1> offline :( </h1>',{headers:{'Content-Type':'text/html'}}));
    }
    else{
          // console.log(event.request.url);
    event.respondWith(fetch(event.request));
    }
})


// self.registration.showNotification('Update',{
//     body:'New Bug Ticket Raised',
//     badge:'assets/Image/android-chrome-192x192.png',
//     icon:'assets/Image/android-chrome-192x192.png',
//     // image:'assets/Image/android-chrome-192x192.png',
//     tag:'BRM',
//     renotify:'false',
//     actions:[{ action: 'yes', title: 'Yes', icon:'assets/Image/sync.png'}],
//     //other options
// });


self.addEventListener('notificationclose', evt => {
    log('notification closed');
});

self.addEventListener('notificationclick', evt => {
    // handle notification click
    console.log('[Service Worker] Notification click Received.');
    evt.notification.close();
    switch(evt.action){
        case 'yes':
        console.log('yes action clicked!');
        break;
        default:
        console.warn('${evt.action} action clicked');
        break;
    }
    evt.waitUntil(
        clients.openWindow('https://developers.google.com/web/')
      );
});

self.addEventListener('push', function(event) {
    console.log('[Service Worker] Push Received.');
    console.log(`[Service Worker] Push had this data: "${event.data.text()}"`);
  
    const title = 'Bug Report Notification';
    const options = {
      body: event.data.text(),
      badge:'assets/Image/android-chrome-192x192.png',
      icon:'assets/Image/android-chrome-192x192.png'
    };
  
    event.waitUntil(self.registration.showNotification(title, options));
  });

  self.addEventListener('sync', event => {
      if(event.tag === 'sync-tickets'){
          event.waitUntil(
              openDatabase('bugreport',1)
              .then(evt =>{
                  db=evt.target.result,
                  tickets=db.transaction(['tickets'],'readwrite').objectStore('update');
                  return getData(tickets, r => r.isSynced === false);
              })
              .then(results => {
                  for(result of results) {
                      var body = new Formdata();
                      for(key in result){
                          body.append(key, result[key]);
                      }

                      fetch('/tickets/now',{
                          method:'POST',
                          body: body,
                          credentials:'include'
                      })
                      .then(res => {
                          if(res.ok){
                              result.isSynced=true;
                              db.transaction(['tickets'],'readwrite').objectStore('update').put(result);
                          }
                      });
                  }
              })
          )
      }
  });

function openDatabase(name,version){
return new Promise((resolve,reject) => {
    var idb=indexedDB.open(name,version);
    idb.onsuccess=resolve;
    idb.onerror=reject;
})
}

  function getData(objectStore, predicate){
        return new Promise((resolve,reject) =>{
            var r=[];
            function onsuccess(evt) {
                cursor = evt.target.result;
                if(cursor){
                    if(predicate(cursor.value)){
                        r.push(cursor.value);
                    }
                    cursor.continue();
                }
                else{
                    resolve(r);
                }
            }
            objectStore.openCursor().onsuccess = onsuccess;
        });
  };