﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BRM.Domain.Interfaces.Repositories;
using BRM.Domain.Interfaces.Services;
using BRM.Domain.Entities;
using BRM.Domain.ServiceModels;

namespace BRM.Service
{
    public class TicketService : EntityService<Tickets>, ITicketService
    {
        private readonly ITicketRepository _ticketRepository;

        public TicketService(IUnitOfWork unitOfWork, ITicketRepository ticketRepository) : base(unitOfWork, ticketRepository)
        {
            _ticketRepository = ticketRepository;
        }

        public List<Tickets> GetAllSubscription()
        {
            try
            {
                return _ticketRepository.GetAll().Where(x => x.IsDeleted == false).Select( y => new Tickets {
                    Subscription=y.Subscription,
                    P256DH=y.P256DH,
                    Auth=y.Auth
                }).ToList();
            }
            catch (Exception)
            {
                return null;
                throw;
            }
        }

        public List<Tickets> GetAllTickets()
        {
            try
            {
                return _ticketRepository.GetAll().Where(x=>x.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool SaveSubsciption(Subscription subscription)
        {
            try
            {
                var data = _ticketRepository.GetAll().Where(x => x.IsDeleted == false);
                foreach (var item in data)
                {
                    item.Subscription = subscription.EndPoint;
                    item.P256DH = subscription.Keys.P256DH;
                    item.Auth = subscription.Keys.Auth;
                }
                _ticketRepository.EditListToDb(data);
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }

        public bool SaveTickets(IEnumerable<Tickets> tickets)
        {
            try
            {
                 var data=_ticketRepository.GetAll().Where(x => x.IsDeleted == false);

                var found = 0;
                foreach (var item in data)
                {
                    found = 0;
                    foreach (var newItem in tickets)
                    {
                        if (item.BugId == newItem.BugId)
                        {
                            //Edit
                            item.Subject = newItem.Subject;
                            item.Priority = newItem.Priority;
                            item.Status = newItem.Status;
                            item.ReportedBy = newItem.ReportedBy;
                            item.AssignedTo = newItem.AssignedTo;
                            item.Comments = newItem.Comments;
                            item.CreatedOn = newItem.CreatedOn;
                            found = 1;
                        }
                    }
                    if (found == 0)
                    {
                        //delete
                        item.IsDeleted = true;
                    }
                }

                var ticketsToAdd = tickets.Where(x => !data.Any(y => y.BugId == x.BugId))
                       .Select(s => new Tickets
                       {
                           BugId=s.BugId,
                           Subject=s.Subject,
                           Priority = s.Priority,
                           Status = s.Status,
                           ReportedBy = s.ReportedBy,
                           AssignedTo = s.AssignedTo,
                           Comments = s.Comments,
                           IsDeleted = false
                       }).ToList();

                _ticketRepository.AddListToDb(ticketsToAdd);
                _ticketRepository.EditListToDb(data);
               

                //UnitOfWork.Commit();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }
    }
}
