﻿using System.Configuration;

namespace BRM.Service.Utilities
{
    public static class ConfigUtillity
    {
        public static string Publickey
        {
            get
            {
                return ConfigurationManager.AppSettings["PublicKey"] ?? "";
            }
        }

        public static string Privatekey
        {
            get
            {
                return ConfigurationManager.AppSettings["Privatekey"] ?? "";
            }
        }
    }
}
