﻿using BRM.Domain.Interfaces.Repositories;
using BRM.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BRM.Service
{
    public class EntityService<T> : IEntityService<T> where T : class
    {
        protected IUnitOfWork UnitOfWork;
        protected IGenericRepository<T> Repository;
        public EntityService(IUnitOfWork unitOfWork, IGenericRepository<T> repository)
        {
            UnitOfWork = unitOfWork;
            Repository = repository;
        }

        public bool Create(T entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));

            Repository.Add(entity);
            UnitOfWork.Commit();
            return true;
        }

        public async Task<bool> CreateAsync(T entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            Repository.Add(entity);
            await UnitOfWork.CommitAsync();
            return true;
        }

        public bool Delete(T entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));

            Repository.Delete(entity);
            UnitOfWork.Commit();
            return true;

        }

        public async Task<bool> DeleteAsync(T entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            Repository.Delete(entity);
            await UnitOfWork.CommitAsync();
            return true;

        }

        public IEnumerable<T> GetAll()
        {
            return Repository.GetAll();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await Repository.GetAllAsync();
        }

        public bool Update(T entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));

            Repository.Edit(entity);
            UnitOfWork.Commit();
            return true;
        }

        public bool Update(List<T> entities)
        {
            if (entities != null && entities.Any())
            {
                foreach (var entity in entities)
                {
                    Repository.Edit(entity);
                }
                UnitOfWork.Commit();
                return true;
            }
            else
            {
                throw new ArgumentNullException(nameof(T));
            }
        }

        public async Task<bool> UpdateAsync(T entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));

            Repository.Edit(entity);
            await UnitOfWork.CommitAsync();
            return true;
        }
    }
}
