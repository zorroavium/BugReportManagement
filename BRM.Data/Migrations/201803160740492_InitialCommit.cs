namespace BRM.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCommit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BugComments",
                c => new
                    {
                        BugCommentID = c.Int(nullable: false, identity: true),
                        BugID = c.Int(nullable: false),
                        Comment = c.String(),
                        CommentByID = c.Int(nullable: false),
                        DateTime = c.DateTime(nullable: false),
                        UploadedDocument = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.BugCommentID);
            
            CreateTable(
                "dbo.Bugs",
                c => new
                    {
                        BugID = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 255),
                        PriorityID = c.Int(),
                        ProjectID = c.Int(),
                        StatusID = c.Int(),
                        ReportedByID = c.Int(),
                        ReportedDate = c.DateTime(nullable: false),
                        AssignedToID = c.Int(),
                        AssignedByID = c.Int(),
                        UploadedDocument = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.BugID)
                .ForeignKey("dbo.Projects", t => t.ProjectID)
                .ForeignKey("dbo.ListTypeDetails", t => t.PriorityID)
                .ForeignKey("dbo.ListTypeDetails", t => t.StatusID)
                .Index(t => t.PriorityID)
                .Index(t => t.ProjectID)
                .Index(t => t.StatusID);
            
            CreateTable(
                "dbo.ListTypeDetails",
                c => new
                    {
                        ListTypeDetailID = c.Int(nullable: false, identity: true),
                        ListTypeID = c.Int(nullable: false),
                        ListItemName = c.String(maxLength: 25),
                        SortOrder = c.Byte(nullable: false),
                        Comments = c.String(maxLength: 255),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ListTypeDetailID)
                .ForeignKey("dbo.ListTypes", t => t.ListTypeID, cascadeDelete: true)
                .Index(t => t.ListTypeID);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        DepartmentID = c.Int(nullable: false, identity: true),
                        DepartmentNameID = c.Int(),
                        LocationID = c.Int(),
                        CreatedOn = c.DateTime(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.DepartmentID)
                .ForeignKey("dbo.ListTypeDetails", t => t.DepartmentNameID)
                .Index(t => t.DepartmentNameID);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        EmployeeID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        DesignationID = c.Int(),
                        Username = c.String(),
                        Password = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                        Department_DepartmentId = c.Int(),
                    })
                .PrimaryKey(t => t.EmployeeID)
                .ForeignKey("dbo.Departments", t => t.Department_DepartmentId)
                .Index(t => t.Department_DepartmentId);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        ProjectID = c.Int(nullable: false, identity: true),
                        ProjectName = c.String(),
                        DomainID = c.Int(),
                        CreatedOn = c.DateTime(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ProjectID)
                .ForeignKey("dbo.ListTypeDetails", t => t.DomainID)
                .Index(t => t.DomainID);
            
            CreateTable(
                "dbo.ListTypes",
                c => new
                    {
                        ListTypeID = c.Int(nullable: false, identity: true),
                        ListTypeName = c.String(maxLength: 50),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ListTypeID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Bugs", "StatusID", "dbo.ListTypeDetails");
            DropForeignKey("dbo.Bugs", "PriorityID", "dbo.ListTypeDetails");
            DropForeignKey("dbo.ListTypeDetails", "ListTypeID", "dbo.ListTypes");
            DropForeignKey("dbo.Projects", "DomainID", "dbo.ListTypeDetails");
            DropForeignKey("dbo.Bugs", "ProjectID", "dbo.Projects");
            DropForeignKey("dbo.Departments", "DepartmentNameID", "dbo.ListTypeDetails");
            DropForeignKey("dbo.Employees", "Department_DepartmentId", "dbo.Departments");
            DropIndex("dbo.Projects", new[] { "DomainID" });
            DropIndex("dbo.Employees", new[] { "Department_DepartmentId" });
            DropIndex("dbo.Departments", new[] { "DepartmentNameID" });
            DropIndex("dbo.ListTypeDetails", new[] { "ListTypeID" });
            DropIndex("dbo.Bugs", new[] { "StatusID" });
            DropIndex("dbo.Bugs", new[] { "ProjectID" });
            DropIndex("dbo.Bugs", new[] { "PriorityID" });
            DropTable("dbo.ListTypes");
            DropTable("dbo.Projects");
            DropTable("dbo.Employees");
            DropTable("dbo.Departments");
            DropTable("dbo.ListTypeDetails");
            DropTable("dbo.Bugs");
            DropTable("dbo.BugComments");
        }
    }
}
