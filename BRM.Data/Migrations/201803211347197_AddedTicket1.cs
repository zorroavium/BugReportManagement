namespace BRM.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTicket1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tickets",
                c => new
                    {
                        TicketID = c.Int(nullable: false, identity: true),
                        BugID = c.Int(nullable: false),
                        Subject = c.String(maxLength: 255),
                        Priority = c.String(),
                        Status = c.String(),
                        ReportedBy = c.String(),
                        AssignedTo = c.String(),
                        Comments = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        ModifiedOn = c.DateTime(),
                        ModifiedBy = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.TicketID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Tickets");
        }
    }
}
