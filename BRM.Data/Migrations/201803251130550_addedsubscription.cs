namespace BRM.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedsubscription : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tickets", "Subscription", c => c.String());
            AddColumn("dbo.Tickets", "P256DH", c => c.String());
            AddColumn("dbo.Tickets", "Auth", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tickets", "Auth");
            DropColumn("dbo.Tickets", "P256DH");
            DropColumn("dbo.Tickets", "Subscription");
        }
    }
}
