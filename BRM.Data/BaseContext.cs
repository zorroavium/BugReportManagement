﻿
using BRM.Domain.Entities.Foundation;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading;

namespace BRM.Data
{
    public class BaseContext<TContext> : DbContext where TContext : DbContext
    {
        static BaseContext()
        {
            Database.SetInitializer<TContext>(null);
        }

        protected BaseContext() : base("name=DbConnection")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<BRMDBContext, Configuration>());
        }

        /// <summary>
        /// Overridden the Save changes methods to handle the audit process by making entries 
        /// to CreadedBy, Created Date, ModifiedBy Modified Date columns
        /// </summary>
        /// <returns>
        /// The number of state entries written to the underlying database. This can include
        /// state entries for entities and/or relationships. Relationship state entries are created for
        /// many-to-many relationships and relationships where there is no foreign key property
        /// included in the entity class (often referred to as independent associations).
        /// </returns>
        public override int SaveChanges()
        {

            try
            {
                var identity = Thread.CurrentPrincipal.Identity;
                if (!identity.IsAuthenticated) // Currently no authentication so always false.
                {
                    var modifiedEntries = ChangeTracker.Entries()
                    .Where(x => x.Entity is IAuditableEntity &&
                                (x.State == EntityState.Added || x.State == EntityState.Modified));

                    int userId = 0;
                    foreach (var entry in modifiedEntries)
                    {
                        var entity = entry.Entity as IAuditableEntity;
                        if (entity != null)
                        {
                            ////TODO: NK-  check why the user id is not available from the getUserId
                            //int.TryParse(identity.Name, out userId);
                            var now = DateTime.Now;

                            if (entry.State == EntityState.Added)
                            {
                                entity.CreatedBy = userId;
                                entity.CreatedOn = now;
                                entity.IsDeleted = false;
                            }
                            else
                            {
                                base.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
                                base.Entry(entity).Property(x => x.CreatedOn).IsModified = false;
                                entity.ModifiedBy = userId;
                                entity.ModifiedOn = now;
                            }

                        }
                    }
                }
                return base.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
