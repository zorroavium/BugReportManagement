﻿#region File Header
// /*****************************************************************************************************************
//  * Project Name  :  BRM.Data
//  * File Name  :   BRMDbContext.cs   
//  * Description :  
//  * Created By :   rahul Prasad
//  * Created Date :  16-03-2018
//  * Modified By :  
//  * Last Modified Date :  
//  ****************************************************************************************************************/
#endregion
using System.Data.Entity;
using BRM.Domain.Entities;

namespace BRM.Data
{
    public class BRMDBContext : BaseContext<BRMDBContext>
    {
        public DbSet<Department> Departments { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Bug> Bugs { get; set; }
        public DbSet<ListType> ListTypes { get; set; }
        public DbSet<ListTypeDetail> ListTypeDetails { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<BugComment> BugComments { get; set; }
        public DbSet<Tickets> Tickets { get; set; }
    }
}
