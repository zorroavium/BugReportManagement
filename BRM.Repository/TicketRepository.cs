﻿using BRM.Data;
using BRM.Domain.Entities;
using BRM.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BRM.Repository
{
    public class TicketRepository : GenericRepository<Tickets>, ITicketRepository
    {
        private readonly BRMDBContext _context;

        public TicketRepository(DbContext context) : base(context)
        {
            _context = context as BRMDBContext;
        }

        public void AddListToDb(List<Tickets> ticketsToAdd)
        {
            _context.Tickets.AddRange(ticketsToAdd);
            _context.SaveChanges();
        }

        public void EditListToDb(IEnumerable<Tickets> data)
        {
            foreach (var item in data)
            {
                _context.Entry(item).State = EntityState.Modified;
            }
            _context.SaveChanges();
        }
    }
}
