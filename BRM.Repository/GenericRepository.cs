﻿using BRM.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BRM.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        protected DbContext Context;
        protected DbSet<T> DbSet;

        public GenericRepository(DbContext context)
        {
            Context = context;
            DbSet = Context.Set<T>();
        }

        public IEnumerable<T> GetAll()
        {
            return DbSet.AsEnumerable();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await DbSet.ToListAsync();
        }

        public IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            var query = DbSet.Where(predicate).AsEnumerable();
            return query;
        }

        public int Count(Expression<Func<T, bool>> predicate = null)
        {
            return predicate == null ? DbSet.Count() : DbSet.Count(predicate);
        }

        public bool Any(Expression<Func<T, bool>> predicate = null)
        {
            return predicate == null ? DbSet.Any() : DbSet.Any(predicate);
        }

        public T FirstOrDefault(Expression<Func<T, bool>> predicate = null)
        {
            return predicate == null ? DbSet.FirstOrDefault() : DbSet.FirstOrDefault(predicate);
        }
        public T FirstOrDefaultWithInclude(Expression<Func<T, bool>> predicate = null, string includeProperties = "")
        {
            var query = includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Aggregate<string, IQueryable<T>>(DbSet, (f, includeProperty) => f.Include(includeProperty));
            return predicate == null ? query.FirstOrDefault() : query.FirstOrDefault(predicate);
        }

        public IEnumerable<T> FindWithInclude(Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "",
            int? skip = null, int? take = null)
        {
            IQueryable<T> query = DbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }
            query = includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            if (orderBy != null)
            {
                query = orderBy(query);
            }
            if (skip.HasValue)
            {
                var skipValue = skip.Value;
                query = query.Skip(skipValue);
            }
            if (take.HasValue)
            {
                var takeValue = take.Value;
                query = query.Take(takeValue);
            }
            return query.AsEnumerable();
        }

        public T Add(T entity)
        {
            return DbSet.Add(entity);
        }

        public IEnumerable<T> AddList(IEnumerable<T> entityList)
        {
            return DbSet.AddRange(entityList);
        }

        public T Delete(T entity)
        {
            return DbSet.Remove(entity);
        }

        public IEnumerable<T> DeleteAll(IEnumerable<T> entityList)
        {
            return DbSet.RemoveRange(entityList);
        }

        public void Edit(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
        }

        public void Save()
        {
            Context.SaveChanges();
        }
    }
}
