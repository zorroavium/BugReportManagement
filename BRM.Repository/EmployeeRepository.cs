﻿using BRM.Data;
using BRM.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BRM.Repository
{
    public class EmployeeRepository
    {
        public List<Department> GetDepartMent()
        {
            BRMDBContext brmDBContext = new BRMDBContext();
            return brmDBContext.Departments.ToList();
        }
    }
}
