﻿using BRM.Domain.Entities;
using BRM.Domain.ServiceModels;
using System.Collections.Generic;

namespace BRM.Domain.Interfaces.Services
{
    public interface ITicketService : IEntityService<Tickets>
    {
        List<Tickets> GetAllTickets();
        bool SaveTickets(IEnumerable<Tickets> tickets);
        bool SaveSubsciption(Subscription subscription);
        List<Tickets> GetAllSubscription();
    }
}
