﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BRM.Domain.Interfaces.Services
{
    public interface IEntityService<T>
    {
        /// <summary>
        /// Adds a new Entity of Type T to the database
        /// </summary>
        /// <param name="entity">Entity tobe added</param>
        /// <returns>Boolean</returns>
        bool Create(T entity);

        /// <summary>
        /// Adds a new Entity of Type T to the database asynchronously
        /// </summary>
        /// <param name="entity">Entity tobe added</param>
        /// <returns>Boolean</returns>
        Task<bool> CreateAsync(T entity);

        /// <summary>
        /// Removes an entity from the Database
        /// </summary>
        /// <param name="entity">Entity tobe removed</param>
        /// <returns>Boolean</returns>
        bool Delete(T entity);

        /// <summary>
        /// Removes an entity from the Database asynchronously
        /// </summary>
        /// <param name="entity">Entity tobe removed</param>
        /// <returns>Boolean</returns>
        Task<bool> DeleteAsync(T entity);

        /// <summary>
        /// Fetches all records of Type T
        /// </summary>
        /// <returns>IEnumerable of Records</returns>
        IEnumerable<T> GetAll();

        /// <summary>
        /// Fetches all records of Type T asynchronously
        /// </summary>
        /// <returns>IEnumerable of Records</returns>
        Task<IEnumerable<T>> GetAllAsync();

        /// <summary>
        /// Update an Entity of Type T
        /// </summary>
        /// <param name="entity">Entity to be updated</param>
        /// <returns>Boolean</returns>
        bool Update(T entity);

        /// <summary>
        /// Update List of Entities of Type T
        /// </summary>
        /// <param name="entities">Entities to be updated</param>
        /// <returns>Boolean</returns>
        bool Update(List<T> entities);

        /// <summary>
        /// Update an Entity of Type T asynchronously
        /// </summary>
        /// <param name="entity">Entity to be updated</param>
        /// <returns>Boolean</returns>
        Task<bool> UpdateAsync(T entity);
    }
}
