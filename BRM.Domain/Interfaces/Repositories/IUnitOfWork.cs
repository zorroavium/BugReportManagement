﻿using System;
using System.Threading.Tasks;

namespace BRM.Domain.Interfaces.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        int Commit();
        Task<int> CommitAsync();
        void Dispose(bool disposing);
    }
}
