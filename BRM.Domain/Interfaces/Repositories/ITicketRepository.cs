﻿using BRM.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BRM.Domain.Interfaces.Repositories
{
    public interface ITicketRepository : IGenericRepository<Tickets>
    {
        void AddListToDb(List<Tickets> ticketsToAdd);
        void EditListToDb(IEnumerable<Tickets> data);
    }
}
