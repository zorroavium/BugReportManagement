﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BRM.Domain.Entities.Foundation
{
    public class AuditableEntity<T> : IAuditableEntity
    {
        [ScaffoldColumn(false)]
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        [ScaffoldColumn(false)]
        public DateTime? ModifiedOn { get; set; }
        [ScaffoldColumn(false)]
        public int? ModifiedBy { get; set; }
        [ScaffoldColumn(false)]
        public bool IsDeleted { get; set; }
    }
}
