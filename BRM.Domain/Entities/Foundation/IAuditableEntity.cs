﻿using System;

namespace BRM.Domain.Entities.Foundation
{
    public interface IAuditableEntity
    {
        DateTime CreatedOn { get; set; }
        int CreatedBy { get; set; }
        DateTime? ModifiedOn { get; set; }
        int? ModifiedBy { get; set; }
        bool IsDeleted { get; set; }
    }
}