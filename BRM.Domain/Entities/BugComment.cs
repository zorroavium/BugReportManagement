﻿#region File Header
// /*****************************************************************************************************************
//  * Project Name  :  BRM.Domain
//  * File Name  :   BugComment.cs   
//  * Description :  
//  * Created By :   rahul Prasad
//  * Created Date :  15-03-2018
//  * Modified By :  
//  * Last Modified Date :  
//  ****************************************************************************************************************/
#endregion
using BRM.Domain.Entities.Foundation;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BRM.Domain.Entities
{
    public class BugComment : AuditableEntity<BugComment>
    {
        [Key]
        [Column("BugCommentID")]
        public int BugCommentId { get; set; }
        [Column("BugID")]
        public int BugId { get; set; }
        public string Comment { get; set; }
        [Column("CommentByID")]
        public int CommentById { get; set; }
        public DateTime DateTime { get; set; }
        public string UploadedDocument { get; set; }
    }
}
