﻿#region File Header
// /*****************************************************************************************************************
//  * Project Name  :  BRM.Domain
//  * File Name  :   Department.cs   
//  * Description :  
//  * Created By :   rahul Prasad
//  * Created Date :  15-03-2018
//  * Modified By :  
//  * Last Modified Date :  
//  ****************************************************************************************************************/
#endregion
using BRM.Domain.Entities.Foundation;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BRM.Domain.Entities
{
    public class Department : AuditableEntity<Department>
    {
        [Key]
        [Column("DepartmentID")]
        public int DepartmentId { get; set; }
        [Column("DepartmentNameID")]
        public int? DepartmentNameId { get; set; }
        [Column("LocationID")]
        public int? LocationId { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }
        [ForeignKey("DepartmentNameId")]
        public virtual ListTypeDetail DepartmentName { get; set; }
        [ForeignKey("LocationId")]
        public virtual ListTypeDetail Location { get; set; }
    }
}
