﻿#region File Header
// /*****************************************************************************************************************
//  * Project Name  :  BRM.Domain
//  * File Name  :   ListType.cs   
//  * Description :  
//  * Created By :   rahul Prasad
//  * Created Date :  15-03-2018
//  * Modified By :  
//  * Last Modified Date :  
//  ****************************************************************************************************************/
#endregion
using BRM.Domain.Entities.Foundation;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BRM.Domain.Entities
{
    public class ListType : AuditableEntity<ListType>
    {
        [Key]
        [Column("ListTypeID")]
        public int ListTypeId { get; set; }

        [MaxLength(50)]
        public string ListTypeName { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<ListTypeDetail> ListTypeDetails { get; set; }
    }
}