﻿#region File Header
// /*****************************************************************************************************************
//  * Project Name  :  BRM.Domain
//  * File Name  :   Bug.cs   
//  * Description :  
//  * Created By :   rahul Prasad
//  * Created Date :  15-03-2018
//  * Modified By :  
//  * Last Modified Date :  
//  ****************************************************************************************************************/
#endregion
using BRM.Domain.Entities.Foundation;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BRM.Domain.Entities
{
    public class Bug : AuditableEntity<Bug>
    {
        [Key]
        [Column("BugID")]
        public int BugId { get; set; }
        [MaxLength(255)]
        public string Description { get; set; }
        [Column("PriorityID")]
        public int? PriorityId { get; set; }
        [Column("ProjectID")]
        public int? ProjectId { get; set; }
        [Column("StatusID")]
        public int? StatusId { get; set; }
        [Column("ReportedByID")]
        public int? ReportedById { get; set; }
        public DateTime ReportedDate { get; set; }
        [Column("AssignedToID")]
        public int? AssignedToId { get; set; }
        [Column("AssignedByID")]
        public int? AssignedById { get; set; }
        public string UploadedDocument { get; set; }

        [ForeignKey("ProjectId")]
        public virtual Project Project { get; set; }

        [ForeignKey("PriorityId")]
        public virtual ListTypeDetail Priority { get; set; }
        [ForeignKey("StatusId")]
        public virtual ListTypeDetail Status { get; set; }     

    }
}
