﻿using BRM.Domain.Entities.Foundation;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BRM.Domain.Entities
{
    public class Tickets : AuditableEntity<Tickets>
    {

        [Key]
        [Column("TicketID")]
        public int TicketId { get; set; }
        [Column("BugID")]
        public int BugId { get; set; }
        [MaxLength(255)]
        public string Subject { get; set; }
        public string Priority { get; set; }
        public string Status { get; set; }
        public string ReportedBy { get; set; }
        public string AssignedTo { get; set; }
        public string Comments { get; set; }
        public string Subscription { get; set; }
        public string P256DH { get; set; }
        public string Auth { get; set; }
    }
}
