﻿#region File Header
// /*****************************************************************************************************************
//  * Project Name  :  BRM.Domain
//  * File Name  :   Employee.cs   
//  * Description :  
//  * Created By :   rahul Prasad
//  * Created Date :  15-03-2018
//  * Modified By :  
//  * Last Modified Date :  
//  ****************************************************************************************************************/
#endregion
using BRM.Domain.Entities.Foundation;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BRM.Domain.Entities
{
    public class Employee : AuditableEntity<Employee>
    {
        [Key]
        [Column("EmployeeID")]
        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [Column("DesignationID")]
        public int? DesignationId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public virtual Department Department { get; set; }
    }
}
