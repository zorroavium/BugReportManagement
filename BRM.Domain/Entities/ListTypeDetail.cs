﻿#region File Header
// /*****************************************************************************************************************
//  * Project Name  :  BRM.Domain
//  * File Name  :   ListTypeDetail.cs   
//  * Description :  
//  * Created By :   rahul Prasad
//  * Created Date :  15-03-2018
//  * Modified By :  
//  * Last Modified Date :  
//  ****************************************************************************************************************/
#endregion
using BRM.Domain.Entities.Foundation;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BRM.Domain.Entities
{
    public class ListTypeDetail : AuditableEntity<ListTypeDetail>
    {
        [Key]
        [Column("ListTypeDetailID")]
        public int ListTypeDetailId { get; set; }
        [Column("ListTypeID")]
        public int ListTypeId { get; set; }
        [MaxLength(25)]
        public string ListItemName { get; set; }
        public byte SortOrder { get; set; }
        [MaxLength(255)]
        public string Comments { get; set; }
        public bool IsActive { get; set; }

        [ForeignKey("ListTypeId")]
        public virtual ListType ListType { get; set; }

        [InverseProperty("Priority")]
        public virtual ICollection<Bug> Priority { get; set; }
        [InverseProperty("Status")]
        public virtual ICollection<Bug> Status { get; set; }
        [InverseProperty("DepartmentName")]
        public virtual ICollection<Department> DepartmentName { get; set; }
        [InverseProperty("Domain")]
        public virtual ICollection<Project> Domain { get; set; }
        [InverseProperty("Location")]
        public virtual ICollection<Department> Location { get; set; }
    }
}
