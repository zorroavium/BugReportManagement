﻿#region File Header
// /*****************************************************************************************************************
//  * Project Name  :  BRM.Domain
//  * File Name  :   Project.cs   
//  * Description :  
//  * Created By :   rahul Prasad
//  * Created Date :  15-03-2018
//  * Modified By :  
//  * Last Modified Date :  
//  ****************************************************************************************************************/
#endregion
using BRM.Domain.Entities.Foundation;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BRM.Domain.Entities
{
    public class Project : AuditableEntity<Project>
    {
        [Key]
        [Column("ProjectID")]
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        [Column("DomainID")]
        public int? DomainId { get; set; }

        public virtual ICollection<Bug> Bug { get; set; }
        [ForeignKey("DomainId")]
        public virtual ListTypeDetail Domain { get; set; }
    }
}
