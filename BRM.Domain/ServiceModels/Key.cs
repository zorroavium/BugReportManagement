﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BRM.Domain.ServiceModels
{
    public class Key
    {
        public string P256DH { get; set; }
        public string Auth { get; set; }
    }
}
