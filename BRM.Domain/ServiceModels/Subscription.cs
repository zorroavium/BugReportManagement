﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BRM.Domain.ServiceModels
{
    public class Subscription
    {
        public string EndPoint { get; set; }
        public int ExpirationTime { get; set; }
        public Key Keys { get; set; }
    }
}
